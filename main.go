package main

import (
	"github.com/hajimehoshi/ebiten"
	"image/color"
	"github.com/hajimehoshi/ebiten/ebitenutil"
	"fmt"
	"time"
	"math/rand"
)

const (
	width  = 320
	height = 320
	size   = 16
)

const (
	DirectionStop   = 0
	DirectionTop    = 1
	DirectionRight  = 2
	DirectionBottom = 3
	DirectionLeft   = 4
)

type headStatus struct {
	x         int
	y         int
	direction int
	speed     int
	square    *ebiten.Image
}

type appleStatus struct {
	x      int
	y      int
	square *ebiten.Image
}

var (
	head  headStatus
	apple appleStatus
)

func reset() {
	head = headStatus{
		x:         width / 2,
		y:         height / 2,
		direction: DirectionStop,
		speed:     500,
	}

	randomSetApple()
}

func randomSetApple() {
	rand.Seed(time.Now().UnixNano())
	pos := rand.Intn(width / size * height / size)
	apple.x = pos / (width / size) * size
	apple.y = pos % (width / size) * size
}

func update(screen *ebiten.Image) error {
	if ebiten.IsKeyPressed(ebiten.KeyUp) {
		head.direction = DirectionTop
	}
	if ebiten.IsKeyPressed(ebiten.KeyDown) {
		head.direction = DirectionBottom
	}
	if ebiten.IsKeyPressed(ebiten.KeyLeft) {
		head.direction = DirectionLeft
	}
	if ebiten.IsKeyPressed(ebiten.KeyRight) {
		head.direction = DirectionRight
	}

	if head.square == nil {
		reset()

		head.square, _ = ebiten.NewImage(size, size, ebiten.FilterNearest)
		head.square.Fill(color.White)
	}
	headOpts := &ebiten.DrawImageOptions{}
	headOpts.GeoM.Translate(float64(head.x), float64(head.y))
	screen.DrawImage(head.square, headOpts)

	if apple.square == nil {
		apple.square, _ = ebiten.NewImage(size, size, ebiten.FilterNearest)
		apple.square.Fill(color.RGBA{R: 255, G: 0, B: 0, A: 255})
	}
	appleOpts := &ebiten.DrawImageOptions{}
	appleOpts.GeoM.Translate(float64(apple.x), float64(apple.y))
	screen.DrawImage(apple.square, appleOpts)

	ebitenutil.DebugPrint(screen, fmt.Sprintf("speed: %d", head.speed))

	return nil
}

func main() {
	go func() {
		for {
			switch head.direction {
			case 1:
				head.y = head.y - size
			case 2:
				head.x = head.x + size
			case 3:
				head.y = head.y + size
			case 4:
				head.x = head.x - size
			default:
			}

			//game over
			if head.x < 0 || head.x > width-size || head.y < 0 || head.y > height-size {
				reset()
			}

			//get apple
			if head.x == apple.x && head.y == apple.y {
				randomSetApple()
			}

			if head.direction != 0 && head.speed > 50 {
				head.speed = int(float64(head.speed) * 0.999)
			}
			time.Sleep(time.Duration(head.speed) * time.Millisecond)
		}
	}()

	if err := ebiten.Run(update, width, height, 2, "snake game"); err != nil {
		panic(err)
	}
}
