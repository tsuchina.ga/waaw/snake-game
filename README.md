# _WAAW11_ snake-game

毎週1つWebアプリを作ろうの11回目。  
期間は18/07/20 - 18/07/26  
初歩的なゲームとして、蛇ゲームを作ってみる

## ゴール

* [ ] 蛇ゲームをローカルでプレイできる
* [ ] 蛇ゲームをブラウザからプレイできる

## 目的

* [ ] ゲーム開発というよりは、アプリ開発の基本的なところを知る

## 課題

* [ ] 蛇ゲームのルール
    * [ヘビゲーム - Wikipedia](https://ja.wikipedia.org/wiki/%E3%83%98%E3%83%93%E3%82%B2%E3%83%BC%E3%83%A0)

* [x] ebitenのセットアップとgccをセットアップ
    * [Installation · hajimehoshi/ebiten Wiki](https://github.com/hajimehoshi/ebiten/wiki/Installation)
    * [Windows · hajimehoshi/ebiten Wiki](https://github.com/hajimehoshi/ebiten/wiki/Windows)
    * [~~C言語入門 - MinGW - gcc のインストール - Windows環境 - Webkaru~~](https://webkaru.net/clang/mingw-gcc-install/)
    * [~~C言語入門 - MinGW - gcc の環境設定（パスを通す） - Windows環境 - Webkaru~~](https://webkaru.net/clang/mingw-gcc-environments/)
    * [MinGW-w64 を導入する — しっぽのさきっちょ | text.Baldanders.info](http://text.baldanders.info/remark/2018/03/mingw-w64/)

* [ ] ebitenのチュートリアル
    * [x] [Tutorial:Your first game in Ebiten · hajimehoshi/ebiten Wiki](https://github.com/hajimehoshi/ebiten/wiki/Tutorial%3AYour-first-game-in-Ebiten)
    * [x] [Tutorial:Screen, colors and squares · hajimehoshi/ebiten Wiki](https://github.com/hajimehoshi/ebiten/wiki/Tutorial%3AScreen%2C-colors-and-squares)
    * [x] [Tutorial:Handle user inputs · hajimehoshi/ebiten Wiki](https://github.com/hajimehoshi/ebiten/wiki/Tutorial%3AHandle-user-inputs)
    * [ ] [Tutorial:Playing Sounds · hajimehoshi/ebiten Wiki](https://github.com/hajimehoshi/ebiten/wiki/Tutorial%3APlaying-Sounds)

## 振り返り

0. Idea:

    アイデアやテーマについて
    * ゲーム作りのチュートリアルとしては悪くないはず
    * ウィンドウを作るってレベルからやから時期尚早やった気はする

0. What went right:

    成功したこと・できたこと
    * ウィンドウの作成
    * 流れ

0. What went wrong:

    失敗したこと・できなかったこと
    * シーンやページのような概念
    * 適切な構造化

0. What I learned:

    学べたこと
    * ゲーム作りは難しい
    * フレーム単位で何かが動いてるけど、その何かが分からないということが分かった

## サンプル

なし
